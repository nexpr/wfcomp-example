import { tpl, h } from "../tpl"
import base from "./base"

const head = `
<script src="https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.19/marked.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.min.css">
<style>
#title {margin: 5px 0; width: 100%;}
#body {margin: 5px 0; width: 100%; height: 100px;resize: vertical;}
.btns {text-align: right;}
#preview {margin-top: 10px;}
</style>
`

const body = tpl`
<div><input id="title" value="${"title"}"></div>
<textarea id="body">${"body"}</textarea>
<div class="btns">
	<button id="save">Save</button>
	${function no_data(value) {
		return value
			? ""
			: `
				<button id="delete">Delete</button>
			`
	}}
</div>
<hr/>
<div id="preview" class="markdown-body"></div>
<script>
	const no_data = ${function no_data(value) {
		return JSON.stringify(!!value)
	}}
	const id = ${function id(value) {
		return JSON.stringify(value)
	}}
	const title = document.querySelector("#title")
	const body = document.querySelector("#body")
	// preview
	{
		let tid = null
		body.addEventListener("input", eve => {
			clearTimeout(tid)
			tid = setTimeout(update, 400)
		})
		function update(){
			document.querySelector("#preview").innerHTML = marked(body.value)
		}
		update()
	}
	// post
	{
		document.querySelector("#save").addEventListener("click", eve => {
			confirm("Save?") && fetch("/edit/" + id, {method: "POST", body: JSON.stringify({title: title.value, body: body.value})})
				.then(e => e.json()).then(e => location.href = e.redirect)
		})
		no_data || document.querySelector("#delete").addEventListener("click", eve => {
			confirm("Delete?") && fetch("/delete/" + id, {method: "POST"})
				.then(e => e.json()).then(e => location.href = e.redirect)
		})
	}
</script>
`

export default function(data) {
	return base({
		title: "Edit - " + data.item.id,
		head,
		body: body(data.item),
	})
}
