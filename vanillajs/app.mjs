import http from "http"
import path from "path"
import db from "./db"
import actions from "./actions"

db.init()

const server = http.createServer()
server.on("request", async (req, res) => {
	const url = new URL(path.resolve(req.url), "relative:///")
	const splitted_path = url.pathname.split("/").slice(1)

	const post = new Promise((ok, ng) => {
		if (req.method === "POST") {
			let body = ""
			req.on("data", data => {
				body += data || ""
				// 10MiB 以上の POST は拒否
				if (body.length > 1024 * 1024 * 10) {
					req.connection.destroy()
				}
			})

			req.on("end", () => {
				ok(body)
			})
		} else {
			ok("")
		}
	})

	console.log(req.url)
	try {
		const { action, captures } = findAction(splitted_path, req.method)
		const response = await action({ req, res, query: url.searchParams, post, path: splitted_path, captures })
		// true が返ってくるのは内部でレスポンス処理する場合なので何もしない
		if (response !== true) {
			res.end(response)
		}
	} catch (err) {
		res.end(err.stack)
	}
})
server.listen(9999)

// URL ごとの実行内容を取得
function findAction(path_arr, method) {
	// マッチ処理の途中で正規表現で名前付きキャプチャされたものを保存する変数
	let captures = {}
	
	const conditions = ["path_condition", "method_condition"]
	const matcher = {
		path_condition(cond_path) {
			captures = {}
			return cond_path.every((c, i) => {
				const p = path_arr[i] || ""
				if (typeof c === "string") {
					return c === p
				} else if (c instanceof RegExp) {
					const m = p.match(c)
					if (m) {
						Object.assign(captures, m.groups || {})
					}
					return !!m
				}
				return false
			})
		},
		method_condition(methods) {
			if (Array.isArray(methods)) {
				return methods.includes(method)
			} else {
				return methods === method
			}
		},
	}

	const action = actions.find(item => {
		return conditions.every(cond => {
			if (!(cond in item)) return true
			return matcher[cond](item[cond])
		})
	}).action

	return { action, captures }
}
