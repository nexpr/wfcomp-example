import sqlite3
import os

_dbpath = os.path.realpath("../db/data.sqlite3")


def _dictionarize(cur, row):
    dict = {}
    for idx, col in enumerate(cur.description):
        dict[col[0]] = row[idx]
    return dict


class Db:
    _db = None

    def __init__(self):
        db = sqlite3.connect(_dbpath)
        db.isolation_level = None
        # dictionary 形式で結果を取得
        db.row_factory = _dictionarize
        self._db = db

    def __del__(self):
        self.close()

    def close(self):
        self._db.close()

    def init(self):
        self._db.execute("""
            create table if not exists item (
                id text,
                title text,
                body text,
                date text,
                primary key (id)
            )
        """)

    def get(self, id=None):
        if id is None:
            results = self._db.execute("""
                SELECT *, date(date, "localtime") as jdate FROM item ORDER BY title ASC
            """)
            return results.fetchall()
        else:
            results = self._db.execute("""
                SELECT *, date(date, "localtime") as jdate FROM item WHERE id = ?
            """, [id])
            return results.fetchone()

    def set(self, id, values):
        c = self._db.execute("""
            SELECT count(*) as c FROM item WHERE id = ?
        """, [id]).fetchone()["c"]

        params = {
            "id": id,
            "title": values["title"],
            "body": values["body"],
        }

        if c == 0:
            self._db.execute("""
                INSERT INTO item VALUES (:id, :title, :body, CURRENT_TIMESTAMP) 
            """, params)
        else:
            self._db.execute("""
                UPDATE item SET title = :title, body = :body, date = CURRENT_TIMESTAMP WHERE id = :id
            """, params)

    def delete(self, id):
        self._db.execute("""
            DELETE FROM item WHERE id = ?
        """, [id])

    def search(self, text):
        results = self._db.execute("""
            SELECT * FROM item WHERE id like :word OR title like :word OR body like :word ORDER BY title ASC
        """, {"word": f"%{text}%"})
        return results.fetchall()
