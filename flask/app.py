from flask import Flask, render_template, request, jsonify
from db import Db
import sys

Db().init()
app = Flask(__name__,
            static_url_path="/resources",
            static_folder="resources",
            template_folder="templates",
            )


@app.route("/")
def top():
    rows = Db().get()
    return render_template("top.html", list=rows)


@app.route("/search")
def search():
    query = request.args.get("q", "")
    rows = Db().search(query)
    return render_template("search.html", list=rows, query=query)


@app.route("/view/<id>")
def view(id):
    row = Db().get(id)
    if row:
        return render_template("view.html", item=row)
    else:
        return render_template("error.html", message=f"{id} is not created."), 404


@app.route("/edit/<id>")
def edit(id):
    row = Db().get(id)
    if not row:
        row = {"id": id, "no_data": True}
    return render_template('edit.html', item=row)


@app.route("/edit/<id>", methods=["POST"])
def edit_update(id):
    dict = request.get_json(True)
    Db().set(id, dict)
    return jsonify(redirect=f"/view/{id}")


@app.route("/delete/<id>", methods=["POST"])
def delete(id):
    Db().delete(id)
    return jsonify(redirect="/")


@app.route("/<path:path>")
def error(path):
    return render_template("error.html", message=""), 404


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
