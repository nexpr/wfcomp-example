@extends('base')

@section('title', "Search - {$query}")

@section('head')
<style>
</style>
@endsection

@section('body')
<h1>Search result of "{{ $query }}"</h1>
<div>
	<form>
		<input id="search" name="q" value="{{ $query }}" />
		<button type="submit">Search</button>
	</form>
</div>
<hr>
<ul>
	@foreach ($list as $item)
		<li><a href="/view/{{ $item['id'] }}">{{ $item['title'] }}</a></li>
	@endforeach
</ul>
@endsection
