<?php

namespace App;

class Db
{
    private $db = null;
    public static $instance;

    public function __construct()
    {
        $path = realpath(ROOT . '/../db/data.sqlite3');
        $this->db = new \SQLite3($path);
    }
    
    private static function sql($sql, $params)
    {
        $c = 0;
        return preg_replace_callback('/\\:[a-z_]+|\\?/', function ($m) use ($params, &$c) {
            $key = $m[0] === '?' ? $c++ : substr($m[0], 1);
            $value = $params[$key];
            if (is_string($value)) {
                return "'" .\SQLite3::escapeString((string)$value) . "'";
            } else {
                return (string)$value;
            }
        }, $sql);
    }

    public function init()
    {
        $this->db->query('
            create table if not exists item (
                id text,
                title text,
                body text,
                date text,
                primary key (id)
            )
        ');
    }

    public function get($id = null)
    {
        if ($id === null) {
            $results = $this->db->query('SELECT *, date(date, "localtime") as jdate FROM item ORDER BY title ASC');
            $rows = [];
            while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
                array_push($rows, $row);
            }
            return $rows;
        } else {
            return $this->db->querySingle(self::sql('SELECT *, date(date, "localtime") as jdate FROM item WHERE id = ?', [$id]), true);
        }
    }
    
    public function set($id, $values)
    {
        $c = $this->db->querySingle(self::sql('SELECT count(*) FROM item WHERE id = ?', [$id]));
        $params = [
            'id' => $id,
            'title' => $values['title'],
            'body' => $values['body'],
        ];
        if ($c == 0) {
            $this->db->exec(self::sql('INSERT INTO item VALUES (:id, :title, :body, CURRENT_TIMESTAMP)', $params));
        } else {
            $this->db->exec(self::sql('UPDATE item SET title = $title, body = $body, date = CURRENT_TIMESTAMP WHERE id = :id', $params));
        }
    }
    
    public function delete($id)
    {
        $this->db->exec(self::sql('DELETE FROM item WHERE id = ?', [$id]));
    }

    public function search($text)
    {
        $results = $this->db->query(self::sql(
            'SELECT * FROM item WHERE id like :word OR title like :word OR body like :word ORDER BY title ASC',
            ['word' => "%{$text}%"]
        ));
        $rows = [];
        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            array_push($rows, $row);
        }
        return $rows;
    }
}

Db::$instance = new Db();
