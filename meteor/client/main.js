import db from "./lib/db"

Template.search.events({
	submit(eve, inst) {
		eve.preventDefault()
		const query = document.querySelector("#search").value
		// ? から始めると URL は正しいが表示が / のものになる
		FlowRouter.go("/search?q=" + encodeURIComponent(query))
	},
})

Template.view.events({
	"click #edit"(eve, inst) {
		FlowRouter.go("/edit/" + inst.data.item().id)
	},
})

{
	const update = () => {
		document.querySelector("#preview").innerHTML = marked(document.querySelector("#body").value)
	}

	Template.edit.onRendered(() => {
		update()
	})

	Template.edit.events({
		"input #body"(eve, inst) {
			clearTimeout(inst.tid)
			inst.tid = setTimeout(update, 400)
		},
		async "click #save"(eve, inst) {
			if (confirm("Save?")) {
				const obj = {
					title: document.querySelector("#title").value,
					body: document.querySelector("#body").value,
				}
				const id = inst.data.item().id
				await db.set(id, obj)
				FlowRouter.go("/view/" + id)
			}
		},
		async "click #delete"(eve, inst) {
			if (confirm("Delete?")) {
				const id = inst.data.item().id
				await db.delete(id)
				FlowRouter.go("/")
			}
		},
	})
}
