import { Meteor } from "meteor/meteor"
import db from "./db"

Meteor.startup(() => {
	// code to run on server at startup
	db.init()

	Meteor.methods({
		async db_operation(method_name, ...args){
			return db[method_name](...args)
		}
	})
})
