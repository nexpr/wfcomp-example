import path from "path"
import sqlite3 from "sqlite3"

// ビルドされるのでソースファイルからの相対パスにするのは難しい
// サーバ用リソースの private 以下に置くと "assets/app/data.sqlite3" にコピーされるが
// readonly ファイルになって更新できない
// プロジェクトルートからだと ".meteor/local/build/programs/server/assets/app/data.sqlite3"
// "/tmp" 以下に共通 db ファイルへのシンボリックリンクを置いてそこを参照させて対処
const filepath = "/tmp/data.sqlite3"

console.log("db path: " + filepath)

const db = new (sqlite3.verbose()).Database(filepath)
const toPromise = fn => {
	return new Promise((ok, ng) =>
		fn((err, result) => {
			if (err) ng(err)
			else ok(result)
		})
	)
}

export default {
	async init() {
		db.exec(`
			create table if not exists item (
				id text,
				title text,
				body text,
				date text,
				primary key (id)
			)
		`)
	},
	async get(id) {
		return toPromise(n => {
			if (id) {
				db.get(`SELECT *, datetime(date, "localtime") as jdate FROM item WHERE id = ?`, [id], n)
			} else {
				db.all(`SELECT *, datetime(date, "localtime") as jdate FROM item ORDER BY title ASC`, n)
			}
		})
	},
	async set(id, obj) {
		const { c } = await toPromise(n => db.get("SELECT count(*) as c FROM item WHERE id = ?", [id], n))
		const params = {
			$id: id,
			$title: obj.title,
			$body: obj.body,
		}
		return toPromise(n => {
			if (c === 0) {
				db.run("INSERT INTO item VALUES ($id, $title, $body, CURRENT_TIMESTAMP)", params, n)
			} else {
				db.run("UPDATE item SET title = $title, body = $body, date = CURRENT_TIMESTAMP WHERE id = $id", params, n)
			}
		})
	},
	async delete(id) {
		db.run("DELETE FROM item WHERE id = ?", [id])
	},
	async search(text) {
		return toPromise(n =>
			db.all(
				"SELECT * FROM item WHERE id like $word OR title like $word OR body like $word ORDER BY title ASC",
				{ $word: "%" + text + "%" },
				n
			)
		)
	},
}
