<?php

require_once ROOT . '/db.php';
require_once ROOT . '/tpl.php';

return [
    [
        'title' => 'top',
        'path_condition' => [''],
        'action' => function () {
            $rows = Db::$instance->get();
            return (new Tpl(ROOT . '/templates/top.php'))->render(['list' => $rows]);
        },
    ],
    [
        'title' => 'search',
        'path_condition' => ['search'],
        'method_condition' => 'GET',
        'action' => function () {
            $query = array_key_exists('q', $_GET) ? $_GET['q'] : '';
            $rows = Db::$instance->search($query);
            return (new Tpl(ROOT . '/templates/search.php'))->render(['list' => $rows, 'query' => $query]);
        },
    ],
    [
        'title' => 'view',
        'path_condition' => ['view', '/^(?<id>.+)/'],
        'action' => function ($args) {
            $row = Db::$instance->get($args['captures']['id']);
            if ($row) {
                return (new Tpl(ROOT . '/templates/view.php'))->render(['item' => $row]);
            } else {
                header('HTTP/1.1 404 Not Found');
                return (new Tpl(ROOT . '/templates/error.php'))->render(['message' => "{$args['captures']['id']} is not created."]);
            }
        },
    ],
    [
        'title' => 'edit',
        'path_condition' => ['edit', '/^(?<id>.+)/'],
        'method_condition' => 'GET',
        'action' => function ($args) {
            $row = Db::$instance->get($args['captures']['id']);
            if (!$row) {
                $row = ['id' => $args['captures']['id'], 'no_data' => true];
            }
            return (new Tpl(ROOT . '/templates/edit.php'))->render(['item' => $row]);
        }
    ],
    [
        'title' => 'edit_update',
        'path_condition' => ['edit', '/^(?<id>.+)/'],
        'method_condition' => 'POST',
        'action' => function ($args) {
            $post = file_get_contents("php://input");
            $obj = json_decode($post, true);
            $id = $args['captures']['id'];
            Db::$instance->set($id, $obj);
            header('Content-Type: application/json; charset=utf-8');
            return json_encode(['redirect' => "/view/{$id}"]);
        }
    ],
    [
        'title' => 'delete',
        'path_condition' => ['delete', '/^(?<id>.+)/'],
        'method_condition' => 'POST',
        'action' => function ($args) {
            Db::$instance->delete($args['captures']['id']);
            header('Content-Type: application/json; charset=utf-8');
            return json_encode(['redirect' => '/']);
        },
    ],
    [
        'title' => 'default',
        'action' => function () {
            header('HTTP/1.1 404 Not Found');
            return (new Tpl(ROOT . '/templates/error.php'))->render([]);
        },
    ],
];
