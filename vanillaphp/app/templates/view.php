<? usetpl(__DIR__ ."/base.php") ?>

<? setvar("title", h($item['title'])); ?>

<? section("head"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.19/marked.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.min.css">
<style>
#title {margin: 5px 0; width: 100%;}
h1 span {font-size: 12px; color: silver;}
.btns {text-align: right;}
</style>
<? sectionend(); ?>

<? section("body"); ?>
<h1><?= h($item['title']) ?> <span>(<?= h($item['jdate']) ?>)</span></h1>
<div class="btns"><button id="edit">Edit</button></div>
<div id="body" class="markdown-body"></div>
<template id="md"><?= h($item['body']) ?></template>
<script>
	document.querySelector("#body").innerHTML = marked(document.querySelector("#md").content.textContent)
	document.querySelector("#edit").addEventListener("click", eve => location.href = location.href.replace("view", "edit"))
</script>
<? sectionend(); ?>

<? usetplend(); ?>
