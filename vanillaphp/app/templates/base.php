<!doctype html>
<title><?= h($title) ?></title>
<script src="/resources/common.js" type="module"></script>
<link rel="stylesheet" href="/resources/common.css" />
<?= $head ?>
<div id="container">
	<?= $body ?>
</div>
