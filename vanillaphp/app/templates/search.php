<? usetpl(__DIR__ ."/base.php") ?>

<? setvar("title", h($query)); ?>

<? section("head"); ?>
<style>
</style>
<? sectionend(); ?>

<? section("body"); ?>
<h1>Search result of "<?= h($query) ?>"</h1>
<div>
	<form>
		<input id="search" name="q" value="<?= h($query) ?>" />
		<button type="submit">Search</button>
	</form>
</div>
<hr>
<ul>
	<? foreach($list as $item): ?>
		<li><a href="/view/<?= h($item['id']) ?>"><?= h($item['title']) ?></a></li>
	<? endforeach ?>
</ul>
<? sectionend(); ?>

<? usetplend(); ?>
