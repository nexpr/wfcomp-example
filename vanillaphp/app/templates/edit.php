<? usetpl(__DIR__ ."/base.php") ?>

<? setvar("title", 'Edit - ' . h($item['id'])); ?>

<? section("head"); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/marked/0.3.19/marked.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.10.0/github-markdown.min.css">
<style>
#title {margin: 5px 0; width: 100%;}
#body {margin: 5px 0; width: 100%; height: 100px;resize: vertical;}
.btns {text-align: right;}
#preview {margin-top: 10px;}
</style>
<? sectionend(); ?>

<? section("body"); ?>
<div><input id="title" value="<?= h($item['title'] ?? '') ?>"></div>
<textarea id="body"><?= h($item['body'] ?? '') ?></textarea>
<div class="btns">
	<button id="save">Save</button>
	<? if(!($item['no_data'] ?? false)): ?>
		<button id="delete">Delete</button>
	<? endif ?>
</div>
<hr/>
<div id="preview" class="markdown-body"></div>
<script>
	const no_data = <?= json_encode($item['no_data'] ?? false) ?><?= PHP_EOL ?>
	const id = <?= json_encode($item['id']) ?><?= PHP_EOL ?>
	const title = document.querySelector("#title")
	const body = document.querySelector("#body")
	// preview
	{
		let tid = null
		body.addEventListener("input", eve => {
			clearTimeout(tid)
			tid = setTimeout(update, 400)
		})
		function update(){
			document.querySelector("#preview").innerHTML = marked(body.value)
		}
		update()
	}
	// post
	{
		document.querySelector("#save").addEventListener("click", eve => {
			confirm("Save?") && fetch("/edit/" + id, {method: "POST", body: JSON.stringify({title: title.value, body: body.value})})
				.then(e => e.json()).then(e => location.href = e.redirect)
		})
		no_data || document.querySelector("#delete").addEventListener("click", eve => {
			confirm("Delete?") && fetch("/delete/" + id, {method: "POST"})
				.then(e => e.json()).then(e => location.href = e.redirect)
		})
	}
</script>
<? sectionend(); ?>

<? usetplend(); ?>
