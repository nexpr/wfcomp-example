# How to start

- docroot をドキュメントルートにする
- short_open_tag が有効の必要あり


```
LISTEN 8001
<VirtualHost *:8001>
    DocumentRoot /path/to/vanillaphp/docroot
    ServerName server01
    Options FollowSymLinks
    <Directory /path/to/vanillaphp/docroot>
        Options Indexes FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>
</VirtualHost>
```

or

```
php -S 0.0.0.0:9999 -t docroot
```
