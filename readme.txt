web framework compare example

markdown viewer

route
 /                  list
 /search/?q=query   search
 /view/:id          view(show "not created" error if id does't exist)
 /edit/:id          edit(get->show editor, post->update)
 /delete/:id        delete(post only)
 /<other>           404 error

db: save and load 
  id
  title
  body
  date

table
  create table item (
    id text,
    title text,
    body text,
    date text,
    primary key (id)
  )
